// Angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// App
import { AppComponent } from './app.component';
import { LibraryModule } from './library/library.module';
import { CommonModule } from '@angular/common';


@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    LibraryModule
  ],
  declarations: [
    AppComponent

  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
