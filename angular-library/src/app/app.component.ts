import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'lib-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-library';
 formGroup: FormGroup;
 array = [{
   type: 'radio',
   value: true,
   checked: false,
   name: '1_memo',
   id: '1_memo'
 },{
   type: 'radio',
   value: false,
   checked: false,
   name: '1_memo',
   id: '2_memo'
 }];
  constructor(private fb: FormBuilder) {
this.formGroup = fb.group({
  radio: ''
});
  }
}
