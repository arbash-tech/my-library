// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// App
import { RadioGroupComponent } from './radio-group/radio-group.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [
    RadioGroupComponent
  ],
  exports: [
    RadioGroupComponent
  ],
  providers: [],
})
export class LibraryModule { }
