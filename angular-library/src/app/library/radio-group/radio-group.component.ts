// Angular
import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'lib-radio-group',
  templateUrl: './radio-group.component.html',
  styleUrls: ['./radio-group.component.scss']
})
export class RadioGroupComponent implements OnInit {
  /**
   * Type of inputs Group
   */
  @Input('radioType') type: 'radio' | 'text';
  /**
   * Value of input and Label
   */
  @Input('radioValue') value;
  /**
   * Default checked radio
   */
  @Input('isRadioChecked') checked: boolean;
  /**
   * Is Radio autoFocused
   */
  @Input('radioAutoFocus') autoFocus: boolean;
  /**
   * name of input
   */
  @Input('radioName') name: string;
  /**
   * RadioFormControl
   */
  @Input('radioFormControl') formControl: FormControl;
  /**
   * ID of input
   */
  @Input('radioId') id: string;
  /**
   * Type of inputs Group
   */
  @Input('isRadioDisabled') disabled: 'radio';


  constructor() {
  }

  ngOnInit() {
  }

}
